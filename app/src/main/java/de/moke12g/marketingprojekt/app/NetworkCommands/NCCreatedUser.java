package de.moke12g.marketingprojekt.app.NetworkCommands;

import de.moke12g.marketingprojekt.app.UserManager;
import de.moke12g.marketingprojekt.sharedcode.commands.user.PRCCreatedUser;
import de.moke12g.marketingprojekt.sharedcode.sharedmarketingcode;

public class NCCreatedUser implements NetworkCommand {

    @Override
    public String getShort() {
        return new PRCCreatedUser().getShort();
    }

    @Override
    public void execute(String[] args) {
        // In args[0] befindet sich der neue Tag des Nutzers
        if (sharedmarketingcode.debug)
            System.out.println("Inhalt vom Tag vor der Überschreibung durch " + getShort() + "(" + args[0] + "): " + UserManager.getUserTag());
        UserManager.setUserTag(args[0]);
    }
}
