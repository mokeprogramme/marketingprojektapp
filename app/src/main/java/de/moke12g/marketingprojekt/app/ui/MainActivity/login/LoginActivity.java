package de.moke12g.marketingprojekt.app.ui.MainActivity.login;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.snackbar.Snackbar;

import de.moke12g.marketingprojekt.R;
import de.moke12g.marketingprojekt.app.DatabaseManager;
import de.moke12g.marketingprojekt.app.NetworkManager;
import de.moke12g.marketingprojekt.app.SecurityManager;
import de.moke12g.marketingprojekt.app.StatusManager;
import de.moke12g.marketingprojekt.app.UserManager;
import de.moke12g.marketingprojekt.app.ui.MainActivity.reg.regActivity;

public class LoginActivity extends AppCompatActivity {

    EditText nickname;
    EditText userTag;
    EditText password;

    Button login;
    Button registrar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        NetworkManager.lastLogin = -1;
        final Activity runningActivity = this;

        nickname = findViewById(R.id.nickname);
        userTag = findViewById(R.id.Tag);
        password = findViewById(R.id.Password);
        login = findViewById(R.id.loginButton);
        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        NetworkManager.lastLogin = -1;
                        runningActivity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                NetworkManager.login(nickname.getText().toString(), userTag.getText().toString(), SecurityManager.sha256(password.getText().toString()));
                            }
                        });
                        while (NetworkManager.lastLogin == -1) {
                            try {
                                Thread.sleep(1000);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                        }
                        // Anmeldung hat geklappt, was nun?
                        if (NetworkManager.lastLogin == 1) {
                            runningActivity.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    Snackbar.make(v, getResources().getString(R.string.loginFailed), Snackbar.LENGTH_LONG).show();
                                    // Syncronisiere Userdata
                                    DatabaseManager.createDatabase();
                                    NetworkManager.getFirstNameFromServer();
                                    NetworkManager.getLastNameFromServer();
                                    NetworkManager.getBirthdayFromServer();
                                    NetworkManager.getEMailFromServer();
                                    NetworkManager.getTelephoneNumberFromServer();
                                    UserManager.setNickname(nickname.getText().toString());
                                    UserManager.setUserTag(userTag.getText().toString());
                                    UserManager.setPasswordHash(SecurityManager.sha256(password.getText().toString()));
                                    // Restarte App
                                    DatabaseManager.saveDatabase();
                                    try {
                                        Thread.sleep(3000);
                                    } catch (InterruptedException e) {
                                        e.printStackTrace();
                                    }
                                    StatusManager.triggerRebirth(getApplicationContext());
                                }
                            });
                        } else {
                            //Anmeldung schlug fehl
                            runningActivity.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    Snackbar.make(v, getResources().getString(R.string.loginFailed), Snackbar.LENGTH_LONG).show();
                                }
                            });
                        }
                    }
                }, "loginer").start();
            }
        });

        registrar = findViewById(R.id.registerButton);
        registrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), regActivity.class);
                startActivity(intent);
            }
        });
    }

    @Override
    public void onBackPressed() { // Zum Einsperren des Nutzers
        // HAHA, die Methode zum gehen wird mit "nichts" (!=null) überschrieben -> Es gibt kein entkommen
    }
}
