package de.moke12g.marketingprojekt.app.NetworkCommands.Getters;

import de.moke12g.marketingprojekt.app.NetworkCommands.NetworkCommand;
import de.moke12g.marketingprojekt.app.UserManager;
import de.moke12g.marketingprojekt.sharedcode.commands.user.setters.PRCSetTelephoneNumber;

public class NCSetTelephoneNumber implements NetworkCommand {
    @Override
    public String getShort() {
        return new PRCSetTelephoneNumber().getShort();
    }

    @Override
    public void execute(String[] args) {
        UserManager.setTelephoneNumber(args[0]);
    }
}