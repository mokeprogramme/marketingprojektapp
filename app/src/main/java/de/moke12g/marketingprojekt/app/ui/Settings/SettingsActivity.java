package de.moke12g.marketingprojekt.app.ui.Settings;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

import de.moke12g.marketingprojekt.R;
import de.moke12g.marketingprojekt.app.DatabaseManager;
import de.moke12g.marketingprojekt.app.StatusManager;

public class SettingsActivity extends AppCompatActivity {

    Button resetButton;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        resetButton = findViewById(R.id.resetButton);
        resetButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatabaseManager.deleteDatabase();
                StatusManager.triggerRebirth(v.getContext());
            }
        });

        /*FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });*/
    }


    @Override
    public void onBackPressed() { // Zum Einsperren des Nutzers
        DatabaseManager.saveDatabase();
        //super();
    }
}
