package de.moke12g.marketingprojekt.app;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;

import de.creeperit.lmcprotocol.ArgumentManager;
import de.creeperit.lmcprotocol.CommandBuilder;
import de.creeperit.lmcprotocol.PRCCommands.prcCommand;
import de.creeperit.lmcprotocol.Protocol;
import de.creeperit.lmcprotocol.packedCommand;
import de.moke12g.marketingprojekt.app.NetworkCommands.Getters.NCSetBirthday;
import de.moke12g.marketingprojekt.app.NetworkCommands.Getters.NCSetEMailAddress;
import de.moke12g.marketingprojekt.app.NetworkCommands.Getters.NCSetFirstName;
import de.moke12g.marketingprojekt.app.NetworkCommands.Getters.NCSetLastName;
import de.moke12g.marketingprojekt.app.NetworkCommands.Getters.NCSetTelephoneNumber;
import de.moke12g.marketingprojekt.app.NetworkCommands.NCCreatedUser;
import de.moke12g.marketingprojekt.app.NetworkCommands.NCLoginFailed;
import de.moke12g.marketingprojekt.app.NetworkCommands.NCLoginSuccessfully;
import de.moke12g.marketingprojekt.app.NetworkCommands.NetworkCommand;
import de.moke12g.marketingprojekt.sharedcode.commands.session.PRCLogin;
import de.moke12g.marketingprojekt.sharedcode.commands.user.PRCCreateUser;
import de.moke12g.marketingprojekt.sharedcode.commands.user.getters.PRCGetBirthday;
import de.moke12g.marketingprojekt.sharedcode.commands.user.getters.PRCGetEMailAddress;
import de.moke12g.marketingprojekt.sharedcode.commands.user.getters.PRCGetFirstname;
import de.moke12g.marketingprojekt.sharedcode.commands.user.getters.PRCGetLastName;
import de.moke12g.marketingprojekt.sharedcode.commands.user.getters.PRCGetTelephoneNumber;
import de.moke12g.marketingprojekt.sharedcode.commands.user.setters.PRCSetBirthday;
import de.moke12g.marketingprojekt.sharedcode.commands.user.setters.PRCSetEMailAddress;
import de.moke12g.marketingprojekt.sharedcode.commands.user.setters.PRCSetFirstname;
import de.moke12g.marketingprojekt.sharedcode.commands.user.setters.PRCSetLastName;
import de.moke12g.marketingprojekt.sharedcode.commands.user.setters.PRCSetTelephoneNumber;
import de.moke12g.marketingprojekt.sharedcode.sharedmarketingcode;

public class NetworkManager {

    public static boolean isLoggedIn = false;
    public static int lastLogin = -1;
    private static DataInputStream dis;
    private static Thread thread;
    private static Socket server;
    private static DataOutputStream dos;
    private static HashMap<prcCommand, NetworkCommand> networkCommands;

    public static void login(final String nickname, final String userTag, final String passwordHash) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                getConnection();
                ArrayList<String> args = new ArrayList<>();
                args.add(nickname);
                args.add(userTag);
                args.add(passwordHash);
                write(CommandBuilder.build(new PRCLogin(), ArgumentManager.createArgumentFromArrayList(args)));
            }
        }).start();
    }

    public static void reg(final String nickname, final String passwordHash) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                getConnection();
                UserManager.clearUserTag(); // Ganz wichtig++;
                ArrayList<String> args = new ArrayList<>();
                args.add(nickname);
                args.add(passwordHash);
                write(CommandBuilder.build(new PRCCreateUser(), ArgumentManager.createArgumentFromArrayList(args)));
                flush(); // TODO: WICHTIG -> Wie bekommt die App den Tag für den Account? Dieser ist zum anmelden unabdingbar!
            }
        }).start();
    }

    static void write(final String s) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    if (sharedmarketingcode.debug) System.out.println("Schreibe an Server " + s);
                    dos.writeUTF(s);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }

    static void write(packedCommand packedCommand) {
        write(packedCommand.getFullCommand());
    }

    public static void getConnection() {
        if (thread == null || !thread.isAlive()) {
            isLoggedIn = false;
            sharedmarketingcode.setupProtocol();
            initCommands();
            connectionThreadAsync();
        }
    }

    private static void initCommands() {
        networkCommands = new HashMap<>();
        // Zuordnung von NetworkCommands und prcCommands
        // Für eine Zuordnung wird hier eine Hashmap mit allen prcCommands und den dazugehörigen Shorts erstellt
        HashMap<String, prcCommand> shortsList = new HashMap<>();
        for (prcCommand prcCommand : Protocol.prcCommandList) {
            shortsList.put(prcCommand.getShort(), prcCommand);
        }
        ArrayList<NetworkCommand> commandsToMatch = new ArrayList<>();
        commandsToMatch.add(new NCCreatedUser());
        commandsToMatch.add(new NCLoginFailed());
        commandsToMatch.add(new NCLoginSuccessfully());
        commandsToMatch.add(new NCSetBirthday());
        commandsToMatch.add(new NCSetEMailAddress());
        commandsToMatch.add(new NCSetFirstName());
        commandsToMatch.add(new NCSetLastName());
        commandsToMatch.add(new NCSetTelephoneNumber());
        // weiterer Platz um mehr NetworkCommands hinzuzufügen

        for (NetworkCommand networkCommand : commandsToMatch) {
            networkCommands.put(shortsList.get(networkCommand.getShort()), networkCommand);
        }
        // Jetzt kann für jeden prcCommand der NetworkCommand ermittelt werden
    }

    private static void connectionThreadAsync() {
        // Initialize Server
        thread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    server = new Socket(sharedmarketingcode.serverHostname, sharedmarketingcode.serverPort);
                    dos = new DataOutputStream(server.getOutputStream());
                    dis = new DataInputStream(server.getInputStream());
                } catch (IOException e) {
                    e.printStackTrace(); //TODO: Do things if you cannot connect -> anything
                }
                while (thread.isAlive()) {
                    try { // Compresse Inbox from lmcServer
                        packedCommand packedCommand = CommandBuilder.getPackedCommandFromString(dis.readUTF());
                        if (sharedmarketingcode.debug) { // A debug Box for developers Android Version
                            System.out.println("Command: " + packedCommand.getCommand().getShort() + " (" + packedCommand.getCommand().getLong() + ")");
                            System.out.println("Used ArgumentType: " + packedCommand.getArgumentType().getArgID());
                            System.out.println("Number of Strings: " + packedCommand.getStrings().length);
                            int i = 0;
                            for (String string : packedCommand.getStrings()) {
                                System.out.println(i + ": " + string);
                                i++;
                            }
                        }
                        if (NetworkManager.networkCommands.containsKey(packedCommand.getCommand())) { // Überprüfen ob dieser Command vorhanden ist
                            NetworkManager.networkCommands.get(packedCommand.getCommand()).execute(packedCommand.getStrings());
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        System.out.println("Got Disconnected");
                        thread = new Thread(); // TODO: Füge Timeout Funktion ein
                    }
                }
            }
        });
        thread.setName("Server connection");
        thread.start();
    }

    public static boolean isConnectionThreadOnline() {
        return thread.isAlive();
    }

    public static void flush() {
        new Thread(new Runnable() {
            @Override
            public void run() {

                try {
                    dos.flush();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }

    public static void sendUserDetails() {
        write(CommandBuilder.build(new PRCSetFirstname(), ArgumentManager.createArgumentFromString(UserManager.getFirstName())));
        write(CommandBuilder.build(new PRCSetLastName(), ArgumentManager.createArgumentFromString(UserManager.getLastName())));
        write(CommandBuilder.build(new PRCSetBirthday(), ArgumentManager.createArgumentFromString(UserManager.getBirthday())));
        write(CommandBuilder.build(new PRCSetEMailAddress(), ArgumentManager.createArgumentFromString(UserManager.getEMailAddress())));
        write(CommandBuilder.build(new PRCSetTelephoneNumber(), ArgumentManager.createArgumentFromString(UserManager.getTelephoneNumber())));
        flush();
    }

    public static void getFirstNameFromServer() {
        NetworkManager.write(CommandBuilder.build(new PRCGetFirstname(), ArgumentManager.createArgumentFromString("")));
    }

    public static void getLastNameFromServer() {
        NetworkManager.write(CommandBuilder.build(new PRCGetLastName(), ArgumentManager.createArgumentFromString("")));
    }

    public static void getBirthdayFromServer() {
        NetworkManager.write(CommandBuilder.build(new PRCGetBirthday(), ArgumentManager.createArgumentFromString("")));
    }

    public static void getEMailFromServer() {
        NetworkManager.write(CommandBuilder.build(new PRCGetEMailAddress(), ArgumentManager.createArgumentFromString("")));
    }

    public static void getTelephoneNumberFromServer() {
        NetworkManager.write(CommandBuilder.build(new PRCGetTelephoneNumber(), ArgumentManager.createArgumentFromString("")));
    }
}
