package de.moke12g.marketingprojekt.app.NetworkCommands.Getters;

import de.moke12g.marketingprojekt.app.NetworkCommands.NetworkCommand;
import de.moke12g.marketingprojekt.app.UserManager;
import de.moke12g.marketingprojekt.sharedcode.commands.user.setters.PRCSetLastName;

public class NCSetLastName implements NetworkCommand {
    @Override
    public String getShort() {
        return new PRCSetLastName().getShort();
    }

    @Override
    public void execute(String[] args) {
        UserManager.setLastName(args[0]);
    }
}