package de.moke12g.marketingprojekt.app;

import java.io.File;
import java.io.IOException;

import de.moke12g.MoKeCore.Settings;

public class DatabaseManager {

    public static String settingsName = "userdata";
    static String templatePath = "none";
    static boolean usesTemplate = false;

    static Settings database;
    static Settings template;

    /*

    Aus einem Template wird gelesen, wenn dieser Eintrag in der Settingsdatei nicht vorhanden ist.
    Damit das Template nicht geändert wird, schreiben wir in SettingsName.
    So können wir verschiedene Stände wärend der Präsentation importieren und so schnell die Personen wechseln

     */

    public static boolean userDataExist() {
        return new File(settingsName).exists();
    }

    public static void createDatabase() {
        try {
            new File(settingsName).createNewFile();
            loadDatabase();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void loadDatabase() {
        try {
            database = new Settings(new File(settingsName));
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (database.getEntry("templatePath") != null)
            loadTemplate(database.getEntry("templatePath"));
    }

    static void loadTemplate(String templatePath) {
        try {
            template = new Settings(new File(templatePath));
            DatabaseManager.templatePath = templatePath;
            usesTemplate = true;
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    static void unloadTemplate() {
        template = null;
        usesTemplate = false;
    }

    public static void setEntry(String settingsName, String string) {
        database.setEntry(settingsName, string);
    }

    public static String getEntry(String entryName) {
        if (usesTemplate) {
            if (database.getEntry(entryName) != null) return database.getEntry(entryName);
            else return template.getEntry(entryName);
        } else return database.getEntry(entryName);
    }

    public static void delEntry(String entryName) {
        database.delEntry(entryName);
    }

    public static void saveDatabase() {
        try {
            database.saveSettings();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void deleteDatabase() {
        database = null;
        new File(settingsName).delete();
    }
}
