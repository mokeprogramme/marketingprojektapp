package de.moke12g.marketingprojekt.app.NetworkCommands;

import de.moke12g.marketingprojekt.app.NetworkManager;
import de.moke12g.marketingprojekt.sharedcode.commands.session.PRCLoginFailed;

public class NCLoginFailed implements NetworkCommand {
    @Override
    public String getShort() {
        return new PRCLoginFailed().getShort();
    }

    @Override
    public void execute(String[] args) {
        NetworkManager.lastLogin = 0;
        NetworkManager.isLoggedIn = false;

    }
}
