package de.moke12g.marketingprojekt.app.ui.Starting;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import de.moke12g.marketingprojekt.R;
import de.moke12g.marketingprojekt.app.DatabaseManager;
import de.moke12g.marketingprojekt.app.NetworkManager;
import de.moke12g.marketingprojekt.app.UserManager;
import de.moke12g.marketingprojekt.app.ui.MainActivity.MainActivity;
import de.moke12g.marketingprojekt.app.ui.MainActivity.login.LoginActivity;

public class StartingActivity extends AppCompatActivity {

    Thread starter;
    TextView info;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_starting);

        final Activity myActivity = this;

        info = findViewById(R.id.status);
        info.setText(getResources().getString(R.string.startingApp));

        starter = new Thread(new Runnable() {
            @Override
            public void run() {

                // Erstelle Datenbank
                info.setText(getResources().getString(R.string.creatingDatabase));
                DatabaseManager.settingsName = getFilesDir().toString() + DatabaseManager.settingsName; // WICHTIG!!!

                if (!DatabaseManager.userDataExist()) { // Um UserData zu erstellen

                    // Verbinde zum Server
                    myActivity.runOnUiThread(new Runnable() {
                        public void run() {
                            info.setText(getResources().getString(R.string.connectingToServer));
                        }
                    });
                    NetworkManager.getConnection(); //TODO: Fehlerausgabe wenn Server nicht erreichbar ist

                    // Warte auf Serverantwort
                    myActivity.runOnUiThread(new Runnable() {
                        public void run() {
                            info.setText(getResources().getString(R.string.waitingForServer));
                        }
                    });
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                    myActivity.runOnUiThread(new Runnable() {
                        public void run() {
                            Intent intent = new Intent(myActivity, LoginActivity.class);
                            startActivity(intent);
                        }
                    });
                } else {

                    // Lade die Datenbank
                    myActivity.runOnUiThread(new Runnable() {
                        public void run() {
                            info.setText(getResources().getString(R.string.loadData));
                        }
                    });
                    DatabaseManager.loadDatabase();

                    // Verbinde zum Server
                    myActivity.runOnUiThread(new Runnable() {
                        public void run() {
                            info.setText(getResources().getString(R.string.connectingToServer));
                        }
                    });
                    NetworkManager.getConnection(); //TODO: Fehlerausgabe wenn Server nicht erreichbar ist

                    // Warte auf Serverantwort
                    myActivity.runOnUiThread(new Runnable() {
                        public void run() {
                            info.setText(getResources().getString(R.string.waitingForServer));
                        }
                    });
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                    // Melde User an
                    myActivity.runOnUiThread(new Runnable() {
                        public void run() {
                            info.setText(getResources().getString(R.string.sendingLoginCredentials));
                        }
                    });
                    NetworkManager.login(UserManager.getNickname(), UserManager.getUserTag(), UserManager.getPasswordHash());

                    myActivity.runOnUiThread(new Runnable() {
                        public void run() {
                            info.setText(getResources().getString(R.string.waitingForServer));
                        }
                    });
                    while (NetworkManager.lastLogin == -1) { // Warte auf Antwort
                        try {
                            Thread.sleep(1000);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        if (NetworkManager.lastLogin == 1) {
                            myActivity.runOnUiThread(new Runnable() {
                                public void run() {
                                    info.setText(getResources().getString(R.string.loginSuccessfully));
                                    try {
                                        Thread.sleep(1000);
                                    } catch (InterruptedException e) {
                                        e.printStackTrace();
                                    }
                                    Intent intent = new Intent(myActivity, MainActivity.class);
                                    startActivity(intent);
                                }
                            });
                        } else {
                            myActivity.runOnUiThread(new Runnable() {
                                public void run() {
                                    info.setText(getResources().getString(R.string.loginFailed));
                                    try {
                                        Thread.sleep(1000);
                                    } catch (InterruptedException e) {
                                        e.printStackTrace();
                                    }
                                    Intent intent = new Intent(myActivity, LoginActivity.class);
                                    startActivity(intent);
                                }
                            });
                        }
                    }
                }
            }
        });
        starter.setName("Starter Thread");
        starter.start();
    }
}
