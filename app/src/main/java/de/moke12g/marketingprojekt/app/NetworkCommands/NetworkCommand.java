package de.moke12g.marketingprojekt.app.NetworkCommands;

public interface NetworkCommand {

    String getShort();

    void execute(String[] args);

}
