package de.moke12g.marketingprojekt.app;

public class UserManager {

    public static String getNickname() {
        return DatabaseManager.getEntry("nickname");
    }

    public static void setNickname(String nickname) {
        DatabaseManager.setEntry("nickname", nickname);
    }

    public static String getPasswordHash() {
        return DatabaseManager.getEntry("password");
    }

    public static void setPasswordHash(String passwordHash) {
        DatabaseManager.setEntry("passwordHash", passwordHash);
    }

    public static String getUserTag() {
        return DatabaseManager.getEntry("userTag");
    }

    public static void setUserTag(String userTag) {
        DatabaseManager.setEntry("userTag", userTag);
    }

    public static boolean isTagValid() {
        return getUserTag() != null;
    }

    public static boolean isTagValid(String tag) {
        return tag != null;
    }

    public static void clearUserTag() {
        DatabaseManager.delEntry("userTag");
    }

    static String getFirstName() {
        return DatabaseManager.getEntry("firstName");
    }

    public static void setFirstName(String firstName) {
        DatabaseManager.setEntry("firstName", firstName);
    }

    static String getLastName() {
        return DatabaseManager.getEntry("lastName");
    }

    public static void setLastName(String lastName) {
        DatabaseManager.setEntry("lastName", lastName);
    }

    static String getBirthday() { // TODO: als Datum angeben
        return DatabaseManager.getEntry("birthday");
    }

    public static void setBirthday(String birthday) {
        DatabaseManager.setEntry("birthday", birthday);
    }

    static String getEMailAddress() {
        return DatabaseManager.getEntry("email");
    }

    public static void setEMailAddress(String eMailAddress) {
        DatabaseManager.setEntry("email", eMailAddress);
    }

    static String getTelephoneNumber() {
        return DatabaseManager.getEntry("telephoneNumber");
    }

    public static void setTelephoneNumber(String telephoneNumber) {
        DatabaseManager.setEntry("telephoneNumber", telephoneNumber);
    }
}