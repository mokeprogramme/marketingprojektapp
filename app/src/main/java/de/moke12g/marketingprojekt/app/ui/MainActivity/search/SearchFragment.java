package de.moke12g.marketingprojekt.app.ui.MainActivity.search;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import de.moke12g.marketingprojekt.R;

public class SearchFragment extends Fragment {

    int searchResults = 12; // 0 zählt nicht mit
    LinearLayout[] searchLayout;
    private SearchViewModel searchViewModel;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        searchViewModel = new ViewModelProvider.NewInstanceFactory().create(SearchViewModel.class);
        View root = inflater.inflate(R.layout.fragment_search, container, false);

        LinearLayout searchResult1 = root.findViewById(R.id.searchResult1);
        initSearchResults(root);

        /*final TextView textView = root.findViewById(R.id.text_search);
        searchViewModel.getText().observe(getViewLifecycleOwner(), new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                textView.setText(s);
            }
        });*/
        return root;
    }

    private void initSearchResults(View root) {

        // zu wenig Zeit für diese Art von Coding
        // "Irgendwann kehre ich zurück und erledige die Sidequests" ~ Rufus in Deponia Doomsday

    }

    private void setSearchResult(int searchResult, String string, View root) {
        TextView c = root.findViewById(searchLayout[searchResult].getId());
    }
}
