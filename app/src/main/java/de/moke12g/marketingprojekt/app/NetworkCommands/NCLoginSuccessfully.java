package de.moke12g.marketingprojekt.app.NetworkCommands;

import de.moke12g.marketingprojekt.app.NetworkManager;
import de.moke12g.marketingprojekt.sharedcode.commands.session.PRCLoginSuccessfully;

public class NCLoginSuccessfully implements NetworkCommand {
    @Override
    public String getShort() {
        return new PRCLoginSuccessfully().getShort();
    }

    @Override
    public void execute(String[] args) {
        NetworkManager.lastLogin = 1;
        NetworkManager.isLoggedIn = true;
    }
}
