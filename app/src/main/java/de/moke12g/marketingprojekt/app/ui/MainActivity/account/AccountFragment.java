package de.moke12g.marketingprojekt.app.ui.MainActivity.account;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import de.moke12g.marketingprojekt.R;
import de.moke12g.marketingprojekt.app.UserManager;
import de.moke12g.marketingprojekt.app.ui.Settings.SettingsActivity;

public class AccountFragment extends Fragment {

    private ImageView accountPicture;
    private TextView nickname;
    private ImageButton settings;

    private AccountViewModel accountViewModel;

    public View onCreateView(@NonNull final LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        accountViewModel = new ViewModelProvider.NewInstanceFactory().create(AccountViewModel.class);
        View root = inflater.inflate(R.layout.fragment_account, container, false);

        // initialisiere Elemente

        accountPicture = root.findViewById(R.id.accountPicture);
        nickname = root.findViewById(R.id.nickname);
        nickname.setText(UserManager.getNickname() + ":" + UserManager.getUserTag());
        settings = root.findViewById(R.id.settingsButton);
        settings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(inflater.getContext(), SettingsActivity.class);
                startActivity(intent);
            }
        });

        /*final TextView textView = root.findViewById(R.id.text_account);
        accountViewModel.getText().observe(getViewLifecycleOwner(), new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                textView.setText(s);
            }
        });*/
        return root;
    }
}
