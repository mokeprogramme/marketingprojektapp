package de.moke12g.marketingprojekt.app.NetworkCommands.Getters;

import de.moke12g.marketingprojekt.app.NetworkCommands.NetworkCommand;
import de.moke12g.marketingprojekt.app.UserManager;
import de.moke12g.marketingprojekt.sharedcode.commands.user.setters.PRCSetEMailAddress;

public class NCSetEMailAddress implements NetworkCommand {
    @Override
    public String getShort() {
        return new PRCSetEMailAddress().getShort();
    }

    @Override
    public void execute(String[] args) {
        UserManager.setEMailAddress(args[0]);
    }
}