package de.moke12g.marketingprojekt.app.NetworkCommands.Getters;

import de.moke12g.marketingprojekt.app.NetworkCommands.NetworkCommand;
import de.moke12g.marketingprojekt.app.UserManager;
import de.moke12g.marketingprojekt.sharedcode.commands.user.setters.PRCSetBirthday;

public class NCSetBirthday implements NetworkCommand {
    @Override
    public String getShort() {
        return new PRCSetBirthday().getShort();
    }

    @Override
    public void execute(String[] args) {
        UserManager.setBirthday(args[0]);
    }
}
