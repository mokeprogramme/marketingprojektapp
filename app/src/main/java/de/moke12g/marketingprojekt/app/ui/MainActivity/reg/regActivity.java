package de.moke12g.marketingprojekt.app.ui.MainActivity.reg;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.google.android.material.snackbar.Snackbar;

import de.moke12g.marketingprojekt.R;
import de.moke12g.marketingprojekt.app.DatabaseManager;
import de.moke12g.marketingprojekt.app.NetworkManager;
import de.moke12g.marketingprojekt.app.SecurityManager;
import de.moke12g.marketingprojekt.app.StatusManager;
import de.moke12g.marketingprojekt.app.UserManager;
import de.moke12g.marketingprojekt.sharedcode.sharedmarketingcode;

public class regActivity extends AppCompatActivity {

    EditText firstName;
    EditText lastName;
    EditText birthday;
    EditText nickname;
    EditText email;
    EditText telephoneNumber;
    EditText password1;
    EditText password2;
    CheckBox agreed;
    Button start;

    Activity runningActivity;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reg);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        runningActivity = this;

        firstName = findViewById(R.id.firstname);
        lastName = findViewById(R.id.lastname);
        birthday = findViewById(R.id.birthday);
        nickname = findViewById(R.id.nickname);
        email = findViewById(R.id.email);
        telephoneNumber = findViewById(R.id.telephonenumber);
        password1 = findViewById(R.id.password1);
        password2 = findViewById(R.id.password2);
        agreed = findViewById(R.id.checkBox);
        start = findViewById(R.id.start);

        // Setup Network Connection
        NetworkManager.getConnection();

        start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!firstName.getText().toString().equals("")
                        && !lastName.getText().toString().equals("")
                        && !birthday.getText().toString().equals("")
                        && !nickname.getText().toString().equals("")
                        && emailCheck()
                        && passwordCheck()) { //TODO: Füge mehr Überprüfungen hinzu, wenn es mehr Steuerelemente gibt
                    if (agreed.isChecked()) { // Prüfe auf AGB
                        saveEnteredData();
                        NetworkManager.reg(UserManager.getNickname(), UserManager.getPasswordHash()); // Ich hab immer genug Zeit! Und Motivation
                        //NetworkManager.sendUserDetails(); // TODO: Besser aufteilen, das Protokoll scheint damit überfordert zu werden
                        NetworkManager.flush(); //Hamburg Altona
                        // Die Anwendung wartet jetzt auf die Antwort vom Server, damit Sie den Tag des neuen Nutzers bekommt
                        // Erst danach startet Sie sich neu
                        Thread thread = new Thread(new Runnable() {
                            @Override
                            public void run() {
                                while (!UserManager.isTagValid()) {
                                    try {
                                        Thread.sleep(1000);
                                    } catch (InterruptedException e) {
                                        e.printStackTrace();
                                    }
                                }
                                if (sharedmarketingcode.debug)
                                    System.out.println("Theoretisch kann ich mich jetzt beenden. Der Tag ist: " + UserManager.getUserTag());
                                DatabaseManager.saveDatabase();
                                try {
                                    Thread.sleep(1000);
                                } catch (InterruptedException e) {
                                    e.printStackTrace();
                                }
                                StatusManager.triggerRebirth(getApplicationContext());
                            }
                        });
                        thread.setName("Tag Waiter");
                        thread.start(); // nicht vergessen
                    } else {
                        Snackbar.make(v, getResources().getString(R.string.doAgreeToContinue), Snackbar.LENGTH_LONG).show();
                    }
                } else {
                    Snackbar.make(v, getResources().getString(R.string.FillEverythingOutToContinue), Snackbar.LENGTH_LONG).show();
                }
            }

            private boolean passwordCheck() {
                return password1.getText().toString().equals(password2.getText().toString()) && !password1.getText().toString().equals("");
            }

            private boolean emailCheck() {
                return email.getText().toString().indexOf("@") > 0;
            }
        });

        /*FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });*/
    }

    private void saveEnteredData() {
        DatabaseManager.createDatabase();
        UserManager.setFirstName(firstName.getText().toString());
        UserManager.setLastName(lastName.getText().toString());
        UserManager.setBirthday(birthday.getText().toString());
        UserManager.setNickname(nickname.getText().toString());
        UserManager.setEMailAddress(email.getText().toString());
        UserManager.setTelephoneNumber(telephoneNumber.getText().toString());
        UserManager.setPasswordHash(getEncryptedPassword());
        // Tag wird erst nach dem Verbinden mit dem Server ausgetauscht / erzeugt
        // Registrieren funktioniert manchmal, anmdelden überhauptnicht aber diesmal grundlos -> ich verstehe es nicht ber muss langsam mal wieder mit der Schule weiter machen
        DatabaseManager.saveDatabase();
    }

    private String getEncryptedPassword() {
        return SecurityManager.sha256(password1.getText().toString()); // FIXME Das Password wird immer null
    }
}
